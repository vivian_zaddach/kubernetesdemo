FROM openjdk:16-jdk-alpine
ADD /build/libs/kubernetesdemo-0.0.1-SNAPSHOT.jar kubernetesdemo-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "kubernetesdemo-0.0.1-SNAPSHOT.jar"]
