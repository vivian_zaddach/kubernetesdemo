# Simple Spring Boot App with Kubernetes Deployment
## Make docker image available in minikube

### Execute:

`` eval $(minikube docker-env) ``

### And run the following command afterwards:

`` docker build -t hello-webapp:v1 . ``

## Execute deployment in minikube

`` kubectl apply -f deployment.yaml -f service.yaml``