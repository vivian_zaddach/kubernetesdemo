package com.example.kubernetesdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubernetesdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubernetesdemoApplication.class, args);
	}

}
